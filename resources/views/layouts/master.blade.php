<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>OmniStore - Women and Men clothing and accessories </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/img/favicon.png')}}">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('public/css/preloader.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/metisMenu.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/fontAwesome5Pro.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/default.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/style.css')}}">
</head>
@include("partials.header")


@yield('content')


@include("partials.footer")

<!--js here-->

<script src="{{asset('public/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<script src="{{asset('public/js/vendor/jquery-3.6.0.min.js')}}"></script>
<script src="{{asset('public/js/vendor/waypoints.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/js/metisMenu.min.js')}}"></script>
<script src="{{asset('public/js/slick.min.js')}}"></script>
<script src="{{asset('public/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('public/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('public/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('public/js/jquery-ui-slider-range.js')}}"></script>
<script src="{{asset('public/js/ajax-form.js')}}"></script>
<script src="{{asset('public/js/wow.min.js')}}"></script>
<script src="{{asset('public/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('public/js/main.js')}}"></script>
